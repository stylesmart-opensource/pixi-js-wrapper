var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="./jquery/jquery.d.ts"/>
/// <reference path="./pixi.js/pixi.js.d.ts"/>
/// <reference path="./pixi.js/pixi-filters.d.ts"/>
/// <reference path="./List.ts"/>
var ParticleContainer = PIXI.particles.ParticleContainer;
var PixiWrapper = (function () {
    function PixiWrapper(parent, layerSettings) {
        if (parent === void 0) { parent = $("body"); }
        if (layerSettings === void 0) { layerSettings = [{ Name: "Default" }]; }
        this.TextureCache = PIXI.utils.TextureCache;
        this.Position = new V2();
        this.CoordToPixelRatio = 1;
        this._scale = 1;
        this._moveListeners = new List();
        this.Renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, { antialias: true });
        this.Stage = new PIXI.Container();
        this.OnResize();
        parent.append(this.Renderer.view);
        this._world = new PixiDimension("World", layerSettings.length, layerSettings);
        this.Stage.addChild(this._world);
    }
    PixiWrapper.Prepare = function (urls, exec) {
        if (urls.length <= 0) {
            exec(null, null);
        }
        for (var i = 0; i < urls.length; i++) {
            PIXI.loader.add(urls[i]);
        }
        PIXI.loader.load(exec);
    };
    PixiWrapper.prototype.GetTexture = function (name) {
        if (this.TextureCache.hasOwnProperty(name + ".jpg")) {
            return this.TextureCache[name + ".jpg"];
        }
        else if (this.TextureCache.hasOwnProperty(name + ".png")) {
            return this.TextureCache[name + ".png"];
        }
        else {
            return this.TextureCache[name];
        }
    };
    PixiWrapper.prototype.GetWorldLayer = function (name) {
        return this._world.GetLayerByName(name);
    };
    PixiWrapper.prototype.GetSprite = function (name) {
        return new PIXI.Sprite(this.GetTexture(name));
    };
    PixiWrapper.prototype.SetPosition = function (p, round) {
        if (round === void 0) { round = true; }
        this.Position.copy(p);
        if (this.CoordToPixelRatio != 1) {
            this.Position.MultiplyByFactor(this.CoordToPixelRatio);
        }
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Move(this.Position);
        }
    };
    PixiWrapper.prototype.SetZoom = function (scale) {
        this._scale = scale;
        this.Stage.scale.set(scale);
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Zoom(scale);
        }
    };
    PixiWrapper.prototype.EnableGrass = function (texture) {
        this._grass = new PixiGrass(texture, this.Stage);
        this._moveListeners.AddChild(this._grass);
    };
    PixiWrapper.prototype.Render = function () {
        this.Renderer.render(this.Stage);
    };
    PixiWrapper.prototype.OnResize = function () {
        this.Renderer.resize(window.innerWidth, window.innerHeight);
        this._windowPoint = new V2(window.innerWidth, window.innerHeight);
        this.Stage.position.copy(this._windowPoint.Clone().MultiplyByFactor(0.5).Round());
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Zoom(this._scale);
        }
    };
    return PixiWrapper;
}());
var PixiDimension = (function (_super) {
    __extends(PixiDimension, _super);
    function PixiDimension(name, layers, layerSettings) {
        if (layers === void 0) { layers = 1; }
        if (layerSettings === void 0) { layerSettings = []; }
        _super.call(this);
        this._layers = new List();
        this.Name = name;
        for (var i = 0; i < layers; i++) {
            this._layers.push(new PixiLayer(layerSettings[i] ? layerSettings[i] : {
                Name: "Layer " + i
            }));
            this.addChild(this._layers[i]);
        }
    }
    PixiDimension.prototype.GetLayerByName = function (name) {
        return this._layers.Find({ Name: name })[0];
    };
    return PixiDimension;
}(PIXI.Container));
var PixiLayer = (function (_super) {
    __extends(PixiLayer, _super);
    function PixiLayer(layerSetting) {
        _super.call(this);
        this._chunks = {};
        this._chunksEnabled = false;
        this.Name = layerSetting.Name;
        if (layerSetting.ChunksEnabled) {
            this._chunksEnabled = true;
        }
    }
    PixiLayer.prototype.Add = function (elem) {
        this.addChild(elem);
    };
    return PixiLayer;
}(PIXI.Container));
var PixiActor = (function (_super) {
    __extends(PixiActor, _super);
    function PixiActor(texture) {
        _super.call(this, texture);
    }
    return PixiActor;
}(PIXI.Sprite));
var PixiChunk = (function (_super) {
    __extends(PixiChunk, _super);
    function PixiChunk() {
        _super.apply(this, arguments);
    }
    return PixiChunk;
}(PIXI.Container));
var PixiGrass = (function (_super) {
    __extends(PixiGrass, _super);
    function PixiGrass(texture, stage) {
        _super.call(this);
        this._maxSize = 500;
        this._followFactor = 1;
        this._grassTexture = texture;
        this._grassTextureSize = new V2(this._grassTexture.width, this._grassTexture.height);
        this._subContainer = new ParticleContainer(this._maxSize);
        this._stage = stage;
        stage.addChildAt(this, 0);
        this.addChild(this._subContainer);
        this.Zoom();
    }
    PixiGrass.prototype.Zoom = function (scale) {
        if (scale === void 0) { scale = 1; }
        this._subContainer.removeChildren();
        var boundary = new V2(window.innerWidth, window.innerHeight).DivideByFactor(scale * 2).Add(this._grassTextureSize).Ceil();
        var maxCount = boundary.Clone().Divide(this._grassTextureSize).Ceil().MultiplyByFactor(2).GetMaxPossibilities();
        if (maxCount > this._maxSize) {
            this.removeChild(this._subContainer);
            delete this._subContainer;
            this._maxSize = maxCount;
            this._subContainer = new ParticleContainer(this._maxSize);
            this.addChild(this._subContainer);
        }
        for (var x = -boundary.x; x <= boundary.x; x += this._grassTexture.width) {
            for (var y = -boundary.y; y <= boundary.y; y += this._grassTexture.height) {
                var sprite = new PIXI.Sprite(this._grassTexture);
                sprite.position.set(x, y);
                this._subContainer.addChild(sprite);
            }
        }
    };
    PixiGrass.prototype.Move = function (p) {
        this._subContainer.position.copy(p.Clone().Invert().MultiplyByFactor(this._followFactor).Modulo(this._grassTextureSize).Substract(this._grassTextureSize));
    };
    return PixiGrass;
}(PIXI.Container));
var V2 = (function (_super) {
    __extends(V2, _super);
    function V2() {
        _super.apply(this, arguments);
    }
    V2.prototype.Clone = function () {
        return new V2(this.x, this.y);
    };
    V2.prototype.Substract = function (p) {
        this.x -= p.x;
        this.y -= p.y;
        return this;
    };
    V2.prototype.Add = function (p) {
        this.x += p.x;
        this.y += p.y;
        return this;
    };
    V2.prototype.Multiply = function (p) {
        this.x *= p.x;
        this.y *= p.y;
        return this;
    };
    V2.prototype.Divide = function (p) {
        this.x /= p.x;
        this.y /= p.y;
        return this;
    };
    V2.prototype.MultiplyByFactor = function (factor) {
        this.x *= factor;
        this.y *= factor;
        return this;
    };
    V2.prototype.DivideByFactor = function (factor) {
        this.x /= factor;
        this.y /= factor;
        return this;
    };
    V2.prototype.Round = function () {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);
        return this;
    };
    V2.prototype.Floor = function () {
        this.x = Math.floor(this.x);
        this.y = Math.floor(this.y);
        return this;
    };
    V2.prototype.Ceil = function () {
        this.x = Math.ceil(this.x);
        this.y = Math.ceil(this.y);
        return this;
    };
    V2.prototype.Normalize = function () {
        var z = this.GetDistance();
        this.x /= z;
        this.y /= z;
        return this;
    };
    V2.prototype.GetDistance = function (sqrt) {
        if (sqrt === void 0) { sqrt = true; }
        if (sqrt) {
            return Math.sqrt((this.x) * (this.x) + (this.y) * (this.y));
        }
        else {
            return (this.x) * (this.x) + (this.y) * (this.y);
        }
    };
    V2.prototype.GetMaxPossibilities = function () {
        return Math.ceil(this.x * this.y);
    };
    V2.prototype.GetDistanceTo = function (p, sqrt) {
        if (sqrt === void 0) { sqrt = true; }
        if (sqrt) {
            return Math.sqrt((this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y));
        }
        else {
            return (this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y);
        }
    };
    V2.prototype.Modulo = function (p, forcePositive) {
        if (forcePositive === void 0) { forcePositive = true; }
        this.x %= p.x;
        this.y %= p.y;
        if (forcePositive) {
            while (this.x < 0) {
                this.x += Math.abs(p.x);
            }
            while (this.y < 0) {
                this.y += Math.abs(p.y);
            }
        }
        return this;
    };
    V2.prototype.Invert = function () {
        this.x *= -1;
        this.y *= -1;
        return this;
    };
    return V2;
}(PIXI.Point));
//# sourceMappingURL=PixiWrapper.js.map