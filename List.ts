
class List extends Array {
    AddChild(obj):List {
        var index = this.indexOf(obj);
        if (index == -1) {
            this.push(obj);
        }
        return this;
    }
    RemoveChild(obj):List {
        var index = this.indexOf(obj);
        if (index != -1) {
            this.splice(index,1);
        }
        return this;
    }
    Find(search:{[attr:string]:any}):List {
        var found = new List();
        for (var i = 0; i < this.length; i++) {
            var okay = true;
            for (var attr in search) {
                if (search.hasOwnProperty(attr)) {
                    if (!this[i].hasOwnProperty(attr) || this[i][attr] != search[attr]) {
                        okay = false;
                        break;
                    }
                }
            }
            if (okay) {
                found.push(this[i]);
            }
        }
        return found;
    }
}