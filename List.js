var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var List = (function (_super) {
    __extends(List, _super);
    function List() {
        _super.apply(this, arguments);
    }
    List.prototype.AddChild = function (obj) {
        var index = this.indexOf(obj);
        if (index == -1) {
            this.push(obj);
        }
        return this;
    };
    List.prototype.RemoveChild = function (obj) {
        var index = this.indexOf(obj);
        if (index != -1) {
            this.splice(index, 1);
        }
        return this;
    };
    List.prototype.Find = function (search) {
        var found = new List();
        for (var i = 0; i < this.length; i++) {
            var okay = true;
            for (var attr in search) {
                if (search.hasOwnProperty(attr)) {
                    if (!this[i].hasOwnProperty(attr) || this[i][attr] != search[attr]) {
                        okay = false;
                        break;
                    }
                }
            }
            if (okay) {
                found.push(this[i]);
            }
        }
        return found;
    };
    return List;
}(Array));
//# sourceMappingURL=List.js.map