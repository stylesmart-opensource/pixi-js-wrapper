/// <reference path="./PixiWrapper.ts"/>
var Input = (function () {
    function Input() {
    }
    Input.Init = function () {
        // Keyboard
        $(window).keydown(function (e) {
            Input._keysPressed[e.keyCode] = true;
            Input.TriggerKey("KeyDown", e.keyCode);
        });
        $(window).keyup(function (e) {
            Input._keysPressed[e.keyCode] = false;
            Input.TriggerKey("KeyUp", e.keyCode);
        });
        $(window).blur(function () {
            Input._keysPressed = {};
        });
        //Mouse
        $(window).mousedown(function (e) {
            Input._mousePosition.set(e.screenX, e.screenY);
            Input._mouseDown = true;
            Input.Trigger("MouseDown");
        });
        $(window).mouseup(function (e) {
            Input._mouseDown = false;
            Input.Trigger("MouseUp");
        });
        $(window).mousemove(function (e) {
            if (Input._mouseDown) {
                var n = new V2(e.screenX, e.screenY).Substract(Input._mousePosition).Invert();
                Input._mousePosition.set(e.screenX, e.screenY);
                Input.Trigger("MouseMove", n);
            }
        });
    };
    Input.AddKeyDownListener = function (key, f) {
        if (!Input.EventListeners.KeyDown[key]) {
            Input.EventListeners.KeyDown[key] = [];
        }
        Input.EventListeners.KeyDown[key].push(f);
    };
    Input.AddKeyUpListener = function (key, f) {
        if (!Input.EventListeners.KeyUp[key]) {
            Input.EventListeners.KeyUp[key] = [];
        }
        Input.EventListeners.KeyUp[key].push(f);
    };
    Input.IsPressed = function (keyCode) {
        return Input._keysPressed[keyCode] == true;
    };
    Input.Left = function () {
        return (Input.Direction.Arrows && Input.IsPressed(37) || Input.Direction.WASD && Input.IsPressed(65)) ? 1 : 0;
    };
    Input.Right = function () {
        return (Input.Direction.Arrows && Input.IsPressed(39) || Input.Direction.WASD && Input.IsPressed(68)) ? 1 : 0;
    };
    Input.Up = function () {
        return (Input.Direction.Arrows && Input.IsPressed(38) || Input.Direction.WASD && Input.IsPressed(87)) ? 1 : 0;
    };
    Input.Down = function () {
        return (Input.Direction.Arrows && Input.IsPressed(40) || Input.Direction.WASD && Input.IsPressed(83)) ? 1 : 0;
    };
    Input.TriggerKey = function (name, key) {
        if (Input.EventListeners[name][key]) {
            for (var i = 0; i < Input.EventListeners[name][key].length; i++) {
                Input.EventListeners[name][key][i]();
            }
        }
    };
    Input.Trigger = function (name, param) {
        if (param === void 0) { param = null; }
        for (var i = 0; i < Input.EventListeners[name].length; i++) {
            if (param) {
                Input.EventListeners[name][i](param);
            }
            else {
                Input.EventListeners[name][i]();
            }
        }
    };
    Input._keysPressed = {};
    Input._mouseDown = false;
    Input._mousePosition = new V2();
    Input.Direction = {
        WASD: true,
        Arrows: true
    };
    Input.EventListeners = {
        KeyDown: {},
        KeyUp: {},
        MouseDown: [],
        MouseMove: [],
        MouseUp: []
    };
    return Input;
}());
var InputKey;
(function (InputKey) {
    InputKey[InputKey["LEFT"] = 37] = "LEFT";
    InputKey[InputKey["UP"] = 38] = "UP";
    InputKey[InputKey["RIGHT"] = 39] = "RIGHT";
    InputKey[InputKey["DOWN"] = 40] = "DOWN";
})(InputKey || (InputKey = {}));
//# sourceMappingURL=Input.js.map