/// <reference path="./jquery/jquery.d.ts"/>
/// <reference path="./pixi.js/pixi.js.d.ts"/>
/// <reference path="./pixi.js/pixi-filters.d.ts"/>
/// <reference path="./List.ts"/>
import ParticleContainer = PIXI.particles.ParticleContainer;
class PixiWrapper {
    public Renderer:PIXI.WebGLRenderer | PIXI.CanvasRenderer;
    public Stage:PIXI.Container;
    public TextureCache:Object = PIXI.utils.TextureCache;
    public Position:V2 = new V2();
    public CoordToPixelRatio = 1;
    private _scale:number = 1;
    private _grass:PixiGrass;
    private _windowPoint:V2;
    private _moveListeners:List = new List();
    private _world:PixiDimension;

    constructor(parent = $("body"), layerSettings:PixiLayerSetting[] = [{Name: "Default"}]) {
        this.Renderer = PIXI.autoDetectRenderer(window.innerWidth,window.innerHeight, {antialias: true});
        this.Stage = new PIXI.Container();
        this.OnResize();
        parent.append(this.Renderer.view);

        this._world = new PixiDimension("World", layerSettings.length, layerSettings);
        this.Stage.addChild(this._world);
    }

    static Prepare (urls:string[], exec:(loader:PIXI.loaders.Loader, object:any) => void) {
        if (urls.length <= 0) {
            exec(null,null);
        }
        for (var i = 0; i < urls.length; i++) {
            PIXI.loader.add(urls[i]);
        }
        PIXI.loader.load(exec);
    }

    GetTexture(name:string):PIXI.Texture {
        if (this.TextureCache.hasOwnProperty(name+".jpg")) {
            return this.TextureCache[name+".jpg"];
        } else if (this.TextureCache.hasOwnProperty(name+".png")) {
            return this.TextureCache[name+".png"];
        } else {
            return this.TextureCache[name];
        }
    }

    GetWorldLayer(name:string):PixiLayer {
        return this._world.GetLayerByName(name);
    }

    GetSprite(name:string):PIXI.Sprite {
        return new PIXI.Sprite(this.GetTexture(name));
    }

    SetPosition(p:V2, round:boolean = true) {
        this.Position.copy(p);
        if (this.CoordToPixelRatio != 1) {
            this.Position.MultiplyByFactor(this.CoordToPixelRatio);
        }
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Move(this.Position);
        }
    }

    SetZoom(scale) {
        this._scale = scale;
        this.Stage.scale.set(scale);
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Zoom(scale);
        }
    }

    EnableGrass(texture:PIXI.Texture) {
        this._grass = new PixiGrass(texture, this.Stage);
        this._moveListeners.AddChild(this._grass);
    }

    Render() {
        this.Renderer.render(this.Stage);
    }

    OnResize() {
        this.Renderer.resize(window.innerWidth, window.innerHeight);
        this._windowPoint = new V2(window.innerWidth, window.innerHeight);
        this.Stage.position.copy(this._windowPoint.Clone().MultiplyByFactor(0.5).Round());
        for (var i = 0; i < this._moveListeners.length; i++) {
            this._moveListeners[i].Zoom(this._scale);
        }
    }
}

class PixiDimension extends PIXI.Container {
    private _layers:PixiLayer[] = new List();
    public Name:string;
    constructor(name, layers:number=1, layerSettings:PixiLayerSetting[] = []) {
        super();
        this.Name = name;
        for (var i = 0; i < layers; i++) {
            this._layers.push(new PixiLayer(layerSettings[i] ? layerSettings[i] : {
                Name: "Layer "+i
            }));
            this.addChild(this._layers[i]);
        }
    }

    GetLayerByName(name:string) {
        return (this._layers as List).Find({Name:name})[0];
    }

}

class PixiLayer extends PIXI.Container {
    private _chunks:{[coord:string]:PixiChunk} = {};
    public Name:string;
    private _chunksEnabled:boolean = false;
    constructor(layerSetting:PixiLayerSetting) {
        super();
        this.Name = layerSetting.Name;
        if (layerSetting.ChunksEnabled) {
            this._chunksEnabled = true;
        }
    }

    Add(elem:PIXI.Sprite) {
        this.addChild(elem);
    }

}

class PixiActor extends PIXI.Sprite {
    constructor(texture:PIXI.Texture) {
        super(texture);
    }
}

interface PixiLayerSetting {
    Name:string;
    ChunksEnabled?:boolean;
}

class PixiChunk extends PIXI.Container {

}

class PixiGrass extends PIXI.Container {
    private _grassTexture:PIXI.Texture;
    private _grassTextureSize:V2;
    private _subContainer:ParticleContainer;
    private _stage:PIXI.Container;
    private _maxSize = 500;
    private _followFactor = 1;
    constructor(texture:PIXI.Texture, stage:PIXI.Container) {
        super();
        this._grassTexture = texture;
        this._grassTextureSize = new V2(this._grassTexture.width,this._grassTexture.height);
        this._subContainer = new ParticleContainer(this._maxSize);
        this._stage = stage;

        stage.addChildAt(this,0);
        this.addChild(this._subContainer);
        this.Zoom();
    }
    Zoom(scale=1) {
        this._subContainer.removeChildren();
        var boundary = new V2(window.innerWidth,window.innerHeight).DivideByFactor(scale*2).Add(this._grassTextureSize).Ceil();
        var maxCount = boundary.Clone().Divide(this._grassTextureSize).Ceil().MultiplyByFactor(2).GetMaxPossibilities();
        if (maxCount > this._maxSize) {
            this.removeChild(this._subContainer);
            delete this._subContainer;
            this._maxSize = maxCount;
            this._subContainer = new ParticleContainer(this._maxSize);
            this.addChild(this._subContainer);
        }

        for (var x = -boundary.x; x <= boundary.x; x+= this._grassTexture.width) {
            for (var y = -boundary.y; y <= boundary.y; y+= this._grassTexture.height) {
                var sprite = new PIXI.Sprite(this._grassTexture);
                sprite.position.set(x,y);
                this._subContainer.addChild(sprite);
            }
        }
    }
    Move(p:V2) {
        this._subContainer.position.copy(p.Clone().Invert().MultiplyByFactor(this._followFactor).Modulo(this._grassTextureSize).Substract(this._grassTextureSize));
    }
}

class V2 extends PIXI.Point {
    public Clone():V2 {
        return new V2(this.x,this.y);
    }

    public Substract(p: PIXI.Point): V2 {
        this.x -= p.x;
        this.y -= p.y;
        return this;
    }

    public Add(p: PIXI.Point): V2 {
        this.x += p.x;
        this.y += p.y;
        return this;
    }

    public Multiply(p: PIXI.Point): V2 {
        this.x *= p.x;
        this.y *= p.y;
        return this;
    }

    public Divide(p: PIXI.Point): V2 {
        this.x /= p.x;
        this.y /= p.y;
        return this;
    }

    public MultiplyByFactor(factor:number): V2 {
        this.x *= factor;
        this.y *= factor;
        return this;
    }

    public DivideByFactor(factor:number): V2 {
        this.x /= factor;
        this.y /= factor;
        return this;
    }

    public Round(): V2 {
        this.x = Math.round(this.x);
        this.y = Math.round(this.y);
        return this;
    }

    public Floor(): V2 {
        this.x = Math.floor(this.x);
        this.y = Math.floor(this.y);
        return this;
    }

    public Ceil(): V2 {
        this.x = Math.ceil(this.x);
        this.y = Math.ceil(this.y);
        return this;
    }

    public Normalize():V2 {
        var z = this.GetDistance();
        this.x /= z;
        this.y /= z;
        return this;
    }

    public GetDistance(sqrt:boolean = true): number {
        if (sqrt) {
            return Math.sqrt((this.x)*(this.x)+(this.y)*(this.y));
        } else {
            return (this.x)*(this.x)+(this.y)*(this.y);
        }
    }
    public GetMaxPossibilities(): number {
        return Math.ceil(this.x * this.y);
    }

    public GetDistanceTo(p: PIXI.Point, sqrt:boolean = true): number {
        if (sqrt) {
            return Math.sqrt((this.x-p.x)*(this.x-p.x)+(this.y-p.y)*(this.y-p.y));
        } else {
            return (this.x-p.x)*(this.x-p.x)+(this.y-p.y)*(this.y-p.y);
        }
    }

    public Modulo(p: PIXI.Point, forcePositive:boolean = true): V2 {
        this.x %= p.x;
        this.y %= p.y;
        if (forcePositive) {
            while (this.x < 0) {
                this.x += Math.abs(p.x);
            }
            while (this.y < 0) {
                this.y += Math.abs(p.y);
            }
        }
        return this;
    }

    public Invert(): V2 {
        this.x *= -1;
        this.y *= -1;
        return this;
    }
}