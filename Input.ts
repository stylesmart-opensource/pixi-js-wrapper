/// <reference path="./PixiWrapper.ts"/>
class Input {
    static _keysPressed: {[key: number]: boolean} = {};
    static _mouseDown: boolean = false;
    static _mousePosition: V2 = new V2();

    static Direction = {
        WASD: true,
        Arrows: true
    };

    static EventListeners: InputMapping = {
        KeyDown: {},
        KeyUp: {},
        MouseDown: [],
        MouseMove: [],
        MouseUp: [],
    };

    static Init(): void {

        // Keyboard
        $(window).keydown(function (e) {
            Input._keysPressed[e.keyCode] = true;
            Input.TriggerKey("KeyDown", e.keyCode);
        });

        $(window).keyup(function (e) {
            Input._keysPressed[e.keyCode] = false;
            Input.TriggerKey("KeyUp", e.keyCode);
        });

        $(window).blur(function () {
            Input._keysPressed = {};
        });

        //Mouse
        $(window).mousedown(function (e) {
            Input._mousePosition.set(e.screenX, e.screenY);
            Input._mouseDown = true;
            Input.Trigger("MouseDown");
        });
        $(window).mouseup(function (e) {
            Input._mouseDown = false;
            Input.Trigger("MouseUp");
        });
        $(window).mousemove(function (e) {
            if (Input._mouseDown) {
                var n = new V2(e.screenX, e.screenY).Substract(Input._mousePosition).Invert();
                Input._mousePosition.set(e.screenX, e.screenY);
                Input.Trigger("MouseMove", n);
            }
        });
    }

    static AddKeyDownListener(key: number, f: ()=>void): void {
        if (!Input.EventListeners.KeyDown[key]) {
            Input.EventListeners.KeyDown[key] = [];
        }
        Input.EventListeners.KeyDown[key].push(f);
    }

    static AddKeyUpListener(key: number, f: ()=>void): void {
        if (!Input.EventListeners.KeyUp[key]) {
            Input.EventListeners.KeyUp[key] = [];
        }
        Input.EventListeners.KeyUp[key].push(f);
    }

    static IsPressed(keyCode: any) {
        return Input._keysPressed[keyCode] == true;
    }

    static Left():number {
        return (Input.Direction.Arrows && Input.IsPressed(37) || Input.Direction.WASD && Input.IsPressed(65))?1:0;
    }

    static Right():number {
        return (Input.Direction.Arrows && Input.IsPressed(39) || Input.Direction.WASD && Input.IsPressed(68))?1:0;
    }

    static Up():number {
        return (Input.Direction.Arrows && Input.IsPressed(38) || Input.Direction.WASD && Input.IsPressed(87))?1:0;
    }

    static Down():number {
        return (Input.Direction.Arrows && Input.IsPressed(40) || Input.Direction.WASD && Input.IsPressed(83))?1:0;
    }

    static TriggerKey(name: string, key: number) {
        if (Input.EventListeners[name][key]) {
            for (var i = 0; i < Input.EventListeners[name][key].length; i++) {
                Input.EventListeners[name][key][i]();
            }
        }
    }

    static Trigger(name: string, param: any = null) {
        for (var i = 0; i < Input.EventListeners[name].length; i++) {
            if (param) {
                Input.EventListeners[name][i](param);
            } else {
                Input.EventListeners[name][i]();
            }
        }
    }
}

enum InputKey {
    LEFT = 37,
    UP,
    RIGHT,
    DOWN
}

interface InputMapping {
    KeyDown: {[key: number]: (()=>void)[]};
    KeyUp: {[key: number]: (()=>void)[]};
    MouseDown: (()=>void)[];
    MouseMove: ((diff: V2)=>void)[];
    MouseUp: (()=>void)[];
}