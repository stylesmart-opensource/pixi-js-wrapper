##### Step 1: Put this in your HTML file
```html
<script type="text/javascript" src="pixi-js-wrapper/jquery/jquery.min.js"></script>
<script type="text/javascript" src="pixi-js-wrapper/pixi.js/pixi.min.js"></script>
<script type="text/javascript" src="pixi-js-wrapper/pixi.js/filters.js"></script>
<script type="text/javascript" src="pixi-js-wrapper/PixiWrapper.js"></script>
```
##### Step 2: Use the PixiWrapper
```javascript
var pixi = new PixiWrapper();
pixi.prepare(["grass.png"],function(loader,objects){
    pixi.EnableGrass(pixi.GetTexture("grass"));
    pixi.SetPosition(new V2(10,10)); // Set Position
    pixi.SetZoom(0.5) // Set Zoom
});
```